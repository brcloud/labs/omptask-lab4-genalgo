#ifndef MAIN_H
#define MAIN_H
#include <stdlib.h>
#include <iostream>
#include <ctime>
#include <time.h>
#include <string>
#include <fstream>
#include <queue>
#include <omp.h>

#define OUTPUT_PATH "output"
#define PACKING_DATA_OUTPUT_PATH "output/packing_data"

#define PATH_MAX_SIZE 400

#define INSTANCES_PER_FILE 50

#define N_INSTANCE_META_INFO 6
#define PROB_CLASS 0
#define PROB_INSTANCE 1
#define PROB_POP_SIZE 2
#define PROB_N_GEN 3
#define PROB_MUT_RATE 4
#define PROB_TIME_LIM 5

#endif

